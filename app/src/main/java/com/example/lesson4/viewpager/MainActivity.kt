package com.example.lesson4.viewpager

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleEventObserver
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityViewpagerMainBinding
import com.example.lesson4.databinding.FragmentBasicBinding
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityViewpagerMainBinding.inflate(layoutInflater)

        binding.pager.adapter = BasicFragmentStateAdapter(this)

        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            tab.text = position.toString()
        }.attach()

        setTitle(R.string.activity_viewpager_name)
        setContentView(binding.root)
    }
}

class BasicFragmentStateAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 5

    override fun createFragment(position: Int): Fragment {
        BasicFragment().apply {
            arguments = bundleOf(BasicFragment.KEY_LABEL to position.toString())
        }.also {
            return it
        }
    }

}

class BasicFragment: Fragment() {

    companion object {
        const val TAG = "BasicFragment"

        const val KEY_LABEL = "LABEL"
    }

    private val label
        get() = requireArguments().getString(KEY_LABEL)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycle.addObserver(LifecycleEventObserver { _, event ->
            Log.i(TAG, "Fragment $label: ${event.name}")
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        FragmentBasicBinding.inflate(layoutInflater).apply {
            textView.text = label
        }.also {
            return it.root
        }
    }
}