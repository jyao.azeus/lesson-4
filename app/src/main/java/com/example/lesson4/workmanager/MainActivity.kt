package com.example.lesson4.workmanager

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.*
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy {ActivityThreadMainBinding.inflate(layoutInflater)}

    private val workManager by lazy { WorkManager.getInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            val request = OneTimeWorkRequestBuilder<BackgroundWork>()
                .build()
            workManager.enqueue(request)

            val infoLiveData = WorkQuery.Builder
                .fromIds(listOf(request.id))
                .addStates(listOf(WorkInfo.State.SUCCEEDED))
                .build().let {
                    workManager.getWorkInfosLiveData(it)
                }

            val observer = object : Observer<List<WorkInfo>> {
                override fun onChanged(infos: List<WorkInfo>?) {
                    if (infos.isNullOrEmpty()) {
                        return
                    }

                    addResult(infos[0].outputData.getString(BackgroundWork.KEY_RESULT)!!)

                    infoLiveData.removeObserver(this)
                }
            }

            infoLiveData.observe(this, observer)

        }

        setTitle(R.string.activity_workmanager_name)
        setContentView(binding.root)
    }

    fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    class BackgroundWork(appContext: Context, workerParams: WorkerParameters): Worker(appContext, workerParams) {

        companion object {
            const val KEY_RESULT = "RESULT"
        }

        override fun doWork(): Result {
            Thread.sleep(3000)

            val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalDateTime.now())

            val data = Data.Builder()
                .putString(KEY_RESULT, "$resultTime: ${this@BackgroundWork} finished.")
                .build()

            return Result.success(data)
        }
    }
}