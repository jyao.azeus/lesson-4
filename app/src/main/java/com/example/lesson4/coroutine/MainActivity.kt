package com.example.lesson4.coroutine

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityThreadMainBinding.inflate(layoutInflater)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            runWork()
        }

        setTitle(R.string.activity_coroutine_name)
        setContentView(binding.root)
    }

    private fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    private fun runWork() {
        lifecycleScope.launch {
            val result = backgroundWork()
            addResult(result)
        }
    }

    private suspend fun backgroundWork(): String = withContext(Dispatchers.IO) {
        Thread.sleep(3000)

        val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(
            LocalDateTime.now()
        )

        "$resultTime: ${Thread.currentThread()} finished."
    }
}