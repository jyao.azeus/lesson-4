package com.example.lesson4.handler

import android.os.*
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityThreadMainBinding.inflate(layoutInflater)}

    private val backgroundHandlerThread = BackgroundWorkThread()
    private val resultHandler = ResultsHandler(::addResult)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        backgroundHandlerThread.start()

        val resultMessenger = Messenger(resultHandler)

        binding.button.setOnClickListener {
            backgroundHandlerThread.runBackgroundWork(resultMessenger)
        }

        setTitle(R.string.activity_handler_name)
        setContentView(binding.root)
    }

    private fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        backgroundHandlerThread.quit()
    }

}

class ResultsHandler(val listener: (String) -> Unit) : Handler(Looper.getMainLooper()) {

    companion object {
        const val WHAT_RESULT = 1
        const val KEY_RESULT = "RESULT"
    }

    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)

        when (msg.what) {
            WHAT_RESULT -> listener(msg.data.getString(KEY_RESULT)!!)
        }
    }

}

class BackgroundWorkHandler(looper: Looper) : Handler(looper) {

    companion object {
        const val WHAT_RUN_WORK = 1
    }

    override fun handleMessage(msg: Message) {
        when(msg.what) {
            WHAT_RUN_WORK -> {
                Thread.sleep(3000)

                val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(
                    LocalDateTime.now()
                )

                val result = "$resultTime: ${Thread.currentThread()} finished."

                val message = Message.obtain().apply {
                    what = ResultsHandler.WHAT_RESULT
                    data.putString(ResultsHandler.KEY_RESULT, result)
                }

                msg.replyTo.send(message)
            }
        }
    }
}

class BackgroundWorkThread : HandlerThread("BackgroundWorkHandlerThread") {

    lateinit var handler : Handler

    override fun onLooperPrepared() {
        handler = BackgroundWorkHandler(looper)
    }

    fun runBackgroundWork(returnMessenger: Messenger) {
        handler.obtainMessage(BackgroundWorkHandler.WHAT_RUN_WORK).apply {
            replyTo = returnMessenger
        }.sendToTarget()
    }

}