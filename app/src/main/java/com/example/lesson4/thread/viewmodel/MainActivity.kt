package com.example.lesson4.thread.viewmodel

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityThreadMainBinding.inflate(layoutInflater)}
    private val viewModel by viewModels<ResultsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            viewModel.runBackgroundWork()
        }

        viewModel.resultLiveData.observe(this) {
            addResult(it)
        }

        setTitle(R.string.activity_thread_viewmodel_name)
        setContentView(binding.root)
    }

    private fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }
}

class ResultsViewModel : ViewModel() {

    private val _resultLiveData: MutableLiveData<String> = MutableLiveData()

    val resultLiveData: LiveData<String>
        get() = _resultLiveData

    fun runBackgroundWork() {
        BackgroundWork { result ->
            _resultLiveData.postValue(result)
        }.start()
    }
}

private class BackgroundWork(val listener: (String) -> Unit) : Thread() {

    override fun run() {
        sleep(3000)

        val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(
            LocalDateTime.now()
        )

        listener("$resultTime: ${this@BackgroundWork} finished.")
    }
}