package com.example.lesson4.thread

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.lang.ref.WeakReference
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy {ActivityThreadMainBinding.inflate(layoutInflater)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            BackgroundWork(this).start()
        }

        setTitle(R.string.activity_thread_name)
        setContentView(binding.root)
    }

    fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    private class BackgroundWork(act: MainActivity): Thread() {

        private val actRef = WeakReference(act)

        override fun run() {
            sleep(3000)

            actRef.get()?.let {
                val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalDateTime.now())

                it.runOnUiThread {
                    if (!it.isFinishing && !it.isDestroyed) {
                        it.addResult("$resultTime: ${this@BackgroundWork} finished.")
                    }
                }
            }
        }
    }
}