package com.example.lesson4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.lesson4.databinding.ActivityListItemBinding
import com.example.lesson4.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    enum class Samples(val label: String, val activityClass: Class<out Any>) {
        Thread("Thread", com.example.lesson4.thread.MainActivity::class.java),
        ViewModel("Thread [ViewModel]", com.example.lesson4.thread.viewmodel.MainActivity::class.java),
        Handler("Handler", com.example.lesson4.handler.MainActivity::class.java),
        Executor("Executor", com.example.lesson4.executor.MainActivity::class.java),
        AsyncTask("AsyncTask", com.example.lesson4.asynctask.MainActivity::class.java),
        Coroutine("Coroutine", com.example.lesson4.coroutine.MainActivity::class.java),
        WorkManager("WorkManager", com.example.lesson4.workmanager.MainActivity::class.java),
        ViewPager("ViewPager", com.example.lesson4.viewpager.MainActivity::class.java),
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)

        Samples.values().forEach { sample ->
            val itemBinding = ActivityListItemBinding.inflate(layoutInflater)
            itemBinding.root.setOnClickListener {
                Intent(this, sample.activityClass).also { intent ->
                    startActivity(intent)
                }
            }
            itemBinding.text1.text = sample.label
            binding.list.addView(itemBinding.root)
        }

        setContentView(binding.root)
    }
}