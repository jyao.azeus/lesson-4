package com.example.lesson4.executor

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.lang.ref.WeakReference
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private val binding by lazy {ActivityThreadMainBinding.inflate(layoutInflater)}

    private val executor = Executors.newSingleThreadExecutor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            executor.submit(BackgroundWork(this))
        }

        setTitle(R.string.activity_executor_name)
        setContentView(binding.root)
    }

    fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        executor.shutdown()
    }

    private class BackgroundWork(act: MainActivity): Runnable {

        private val actRef = WeakReference(act)

        override fun run() {
            Thread.sleep(3000)

            actRef.get()?.let {
                val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalDateTime.now())

                it.runOnUiThread {
                    if (!it.isFinishing && !it.isDestroyed) {
                        it.addResult("$resultTime: ${this@BackgroundWork} finished.")
                    }
                }
            }
        }
    }
}