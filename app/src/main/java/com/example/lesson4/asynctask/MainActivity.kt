package com.example.lesson4.asynctask

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.lesson4.R
import com.example.lesson4.databinding.ActivityThreadMainBinding
import java.lang.ref.WeakReference
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MainActivity : AppCompatActivity() {

    private val binding by lazy {ActivityThreadMainBinding.inflate(layoutInflater)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.button.setOnClickListener {
            BackgroundWork(this).execute()
        }

        setTitle(R.string.activity_asynctask_name)
        setContentView(binding.root)
    }

    fun addResult(result: String) {
        binding.textView2.apply {
            append(result)
            append("\n")
        }
    }

    private class BackgroundWork(act: MainActivity) : AsyncTask<Unit, Unit, String>() {
        private val actRef = WeakReference(act)

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Unit?): String {
            Thread.sleep(3000)

            val resultTime = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).format(LocalDateTime.now())

            return "$resultTime: ${this@BackgroundWork} finished."
        }

        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: String) {
            actRef.get()?.let {
                if (!it.isFinishing && !it.isDestroyed) {
                    it.addResult(result)
                }
            }
        }
    }
}